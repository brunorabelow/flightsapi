#!/bin/bash
cd /app
export DJANGO_STATIC_ROOT=/uwsgi/static
./manage.py collectstatic --no-input
./manage.py migrate

# não é legal fazer isso em prod
#./manage.py runserver 0.0.0.0:8000

uwsgi --ini uwsgi.ini
