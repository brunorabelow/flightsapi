FROM python:3.8.2
ENV PYTHONUNBUFFERED 1
RUN apt-get update && \
    apt-get install --no-install-recommends -y nano git locales uwsgi && \
    pip install uwsgi uwsgitop && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/
RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app/
RUN pip install -r requirements.txt
COPY . /app/

COPY docker/bin/* /usr/local/bin/
COPY docker/uwsgi.ini uwsgi.ini
COPY . /app/
