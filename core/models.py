from django.db import models


class Flight(models.Model):
    from_city = models.CharField(max_length=100)
    to_city = models.CharField(max_length=100)
    departure = models.DateTimeField()
    company = models.CharField(max_length=100)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return f'{self.from_city} - {self.to_city} ({self.company}): {self.departure}'
