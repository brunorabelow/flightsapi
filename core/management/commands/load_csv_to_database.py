import csv
from io import BytesIO, TextIOWrapper
from zipfile import ZipFile

from django.core.management import BaseCommand

from core.adapter import google_drive
from core.models import Flight


class Command(BaseCommand):
    help = '''Carrega dados do csv para o banco de dados'''

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        file_id = '1MjTPQc6UEAlpoa5pM4CdvWUyfT_sCN_d'
        response = google_drive.download_file_from_google_drive(id=file_id)
        zip_file = ZipFile(BytesIO(response.content))
        files = zip_file.namelist()
        with zip_file.open(files[0], 'r') as csvfile:
            csvreader = csv.reader(TextIOWrapper(csvfile, encoding='iso-8859-1'))
            header = next(csvreader)
            count = 0
            for row in csvreader:
                data = dict(company=row[1],
                            from_city=row[10],
                            to_city=row[14],
                            departure=row[3])
                Flight.objects.create(**data)
                print(count)
                count += 1
