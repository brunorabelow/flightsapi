from datetime import datetime

import pytz
from django.test import TestCase, Client
from django.utils.timezone import get_default_timezone

from core.models import Flight


class TestFlightsAPI(TestCase):
    @classmethod
    def setUpTestData(cls):
        Flight.objects.create(
            from_city='RIO',
            to_city='SP',
            departure=datetime(year=2020, month=12, day=25, hour=9, minute=0, second=0, tzinfo=get_default_timezone()),
            company='LATAM'
        )
        Flight.objects.create(
            from_city='RIO',
            to_city='SP',
            departure=datetime(year=2020, month=12, day=25, hour=10, minute=0, second=0, tzinfo=get_default_timezone()),
            company='AZUL'
        )
        Flight.objects.create(
            from_city='RIO',
            to_city='SP',
            departure=datetime(year=2020, month=11, day=25, hour=10, minute=0, second=0, tzinfo=get_default_timezone()),
            company='AZUL'
        )
        Flight.objects.create(
            from_city='Nova York',
            to_city='San Diego',
            departure=datetime(year=2020, month=10, day=11, hour=20, minute=30, second=0,
                               tzinfo=get_default_timezone()),
            company='American Airlines'
        )
        Flight.objects.create(
            from_city='Carlos Prates',
            to_city='Confins',
            departure=datetime(year=2020, month=11, day=1, hour=16, minute=30, second=0, tzinfo=get_default_timezone()),
            company='Empresa pequena'
        )

    def test_create_flght(self):
        client = Client()
        params = {
            'from_city': 'Belo Horizonte',
            'departure': '2020-08-01T09:55:00',
            'company': 'Azul'
        }
        r1 = client.post('/flights', params)
        self.assertEqual(400, r1.status_code)

        params = {
            'from_city': 'Belo Horizonte',
            'to_city': 'Rio de Janeiro',
            'departure': '2020-08-01T09:55:00',
            'company': 'Azul'
        }
        r1 = client.post('/flights', params)
        self.assertEqual(201, r1.status_code)

        flight_from_api = r1.json()
        params.update({'id': flight_from_api['id']})
        params.update({'departure': '2020-08-01T09:55:00-03:00'})
        self.assertDictEqual(params, flight_from_api)

        flight_from_db = Flight.objects.get(pk=flight_from_api['id'])
        for key in ['id', 'from_city', 'to_city', 'company']:
            self.assertEqual(getattr(flight_from_db, key, ''), flight_from_api[key])
        self.assertEqual(flight_from_db.departure, datetime.fromisoformat(flight_from_api['departure']))

    def test_search_flights(self):
        client = Client()
        r1 = client.get('/flights?departure=2020-12-25&from_city=RIO&to_city=SC')
        self.assertEqual(200, r1.status_code)
        self.assertListEqual([], r1.json()['results'])

        r2 = client.get('/flights?departure=2020-12-25&from_city=RIO&to_city=SP')
        self.assertEqual(200, r2.status_code)

        flights_from_api = r2.json()['results']
        self.assertEqual(len(flights_from_api), 2)

        self.assertSetEqual({'RIO'}, set([f['from_city'] for f in flights_from_api]))
        self.assertSetEqual({'SP'}, set([f['to_city'] for f in flights_from_api]))
        self.assertSetEqual({datetime(year=2020, month=12, day=25).date()},
                            set([datetime.fromisoformat(f['departure']).date() for f in flights_from_api]))
