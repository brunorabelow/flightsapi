from rest_framework import serializers

from core.models import Flight


class FlightSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    from_city = serializers.CharField(required=True, max_length=100)
    to_city = serializers.CharField(required=True, max_length=100)
    departure = serializers.DateTimeField(required=True)
    company = serializers.CharField(required=True, max_length=100)

    def create(self, validated_data):
        return Flight.objects.create(**validated_data)
