import django_filters
from django.db import models
from django.shortcuts import render

from rest_framework import generics
from rest_framework.pagination import PageNumberPagination

from core.models import Flight
from core.serializers import FlightSerializer
from django_filters import rest_framework as filters


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10


class FlightFilter(filters.FilterSet):
    departure = django_filters.DateFilter(field_name='departure', lookup_expr='date')

    class Meta:
        model = Flight
        fields = ['from_city', 'to_city', 'company', 'departure']


class FlightList(generics.ListCreateAPIView):
    queryset = Flight.objects.all()
    serializer_class = FlightSerializer
    pagination_class = LargeResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = FlightFilter

