from django.contrib import admin

from core.models import Flight

admin.site.register(Flight)
